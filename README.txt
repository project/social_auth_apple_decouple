CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Maintainers

Introduction
------------

This module add a new Social Auth grant type to integrate with Social Auth Apple.


Installation
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

 * Enable module social_auth_apple_decouple

 * Redirect to url to setting information: /admin/config/social-api/social-auth/apple


Maintainers
-----------

 * Tri Tran - https://www.drupal.org/user/3569456
