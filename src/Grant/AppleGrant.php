<?php

namespace Drupal\social_auth_apple_decouple\Grant;

use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\simple_oauth\Entities\UserEntity;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_apple\AppleAuthManager;
use League\OAuth2\Client\Token\AppleAccessToken;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use League\OAuth2\Server\RequestEvent;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * AppleGrant grant class.
 */
class AppleGrant extends AbstractGrant {

  /**
   * Drupal\social_auth_apple\AppleAuthManager definition.
   *
   * @var \Drupal\social_auth_apple\AppleAuthManager
   */
  protected $socialAuthAppleManager;

  /**
   * The network plugin manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  protected $networkManager;

  /**
   * The Social Auth user authenticator..
   *
   * @var \Drupal\social_auth\User\UserAuthenticator
   */
  protected $userAuthenticator;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Extension\ModuleHandler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    UserRepositoryInterface $userRepository,
    RefreshTokenRepositoryInterface $refreshTokenRepository,
    AppleAuthManager $socialAuthAppleManager,
    NetworkManager $networkManager,
    UserAuthenticator $userAuthenticator,
    AccountProxyInterface $currentUser,
    ModuleHandler $moduleHandler
  ) {
    $this->setUserRepository($userRepository);
    $this->setRefreshTokenRepository($refreshTokenRepository);
    $this->socialAuthAppleManager = $socialAuthAppleManager;
    $this->networkManager = $networkManager;
    $this->userAuthenticator = $userAuthenticator;
    $this->currentUser = $currentUser;
    $this->refreshTokenTTL = new \DateInterval('P1M');
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function respondToAccessTokenRequest(
    ServerRequestInterface $request,
    ResponseTypeInterface $responseType,
    \DateInterval $accessTokenTTL
  ) {
    $client = $this->validateClient($request);
    $scopes = $this->validateScopes($this->getRequestParameter('scope', $request, $this->defaultScope));
    $user = $this->validateUser($request, $client);

    $finalizedScopes = $this->scopeRepository->finalizeScopes($scopes, $this->getIdentifier(), $client, $user->getIdentifier());

    $accessToken = $this->issueAccessToken($accessTokenTTL, $client, $user->getIdentifier(), $finalizedScopes);
    $this->getEmitter()
      ->emit(new RequestEvent(RequestEvent::ACCESS_TOKEN_ISSUED, $request));
    $responseType->setAccessToken($accessToken);

    $refreshToken = $this->issueRefreshToken($accessToken);

    if ($refreshToken !== NULL) {
      $this->getEmitter()
        ->emit(new RequestEvent(RequestEvent::REFRESH_TOKEN_ISSUED, $request));
      $responseType->setRefreshToken($refreshToken);
    }

    return $responseType;
  }

  /**
   * Validate user by apple credentials.
   *
   * @param \Psr\Http\Message\ServerRequestInterface $request
   *   Psr\Http\Message\ServerRequestInterface.
   * @param \League\OAuth2\Server\Entities\ClientEntityInterface $client
   *   League\OAuth2\Server\Entities\ClientEntityInterface.
   *
   * @return \League\OAuth2\Server\Entities\UserEntityInterface
   *   League\OAuth2\Server\Entities\UserEntityInterface.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   */
  protected function validateUser(ServerRequestInterface $request, ClientEntityInterface $client) {
    $id_token = $this->getRequestParameter('id_token', $request);
    $code = $this->getRequestParameter('code', $request);

    if (empty($id_token)) {
      throw OAuthServerException::invalidRequest('id_token');
    }
    if (empty($code)) {
      throw OAuthServerException::invalidRequest('code');
    }
    try {
      $this->socialAuthAppleManager->setAccessToken(new AppleAccessToken([
          'refresh_token' => '',
          'access_token' => $code,
          'id_token' => $id_token,
      ]));
      /** @var \League\OAuth2\Client\Provider\AbstractProvider|false $client */
      $client = $this->networkManager->createInstance('social_auth_apple_decouple')
        ->getSdk();
      $this->socialAuthAppleManager->setClient($client);
      /** @var \League\OAuth2\Client\Provider\AppleResourceOwner $profile */
      $profile = $this->socialAuthAppleManager->getUserInfo();
      if (!$profile) {
        throw OAuthServerException::invalidCredentials();
      }

      $this->userAuthenticator->setPluginId('social_auth_apple_decouple');
      $data = $this->userAuthenticator->checkProviderIsAssociated($profile->getId());
      $email = $profile->isPrivateEmail() ? $profile->getId() . '@privaterelay.appleid.com' : $profile->getEmail();
      $this->userAuthenticator->authenticateUser($profile->getFirstName(),
        $email,
        $profile->getId(),
        $this->socialAuthAppleManager->getAccessToken(),
        NULL, $data);
      $this->moduleHandler->invokeAll('social_auth_apple_decouple_after_login', [$profile]);
    }
    catch (\Exception $exception) {
      \Drupal::logger('social_auth_apple_decouple')->debug('148: ' . $exception->getMessage());
      throw new OAuthServerException(
        $exception->getMessage(),
        $exception->getCode(),
        'invalid_apple_credentials',
        400
      );
    }

    if (!$this->currentUser->isAuthenticated()) {
      throw OAuthServerException::invalidCredentials();
    }
    $user = new UserEntity();
    $uid = $this->currentUser->id();
    $user->setIdentifier($uid);

    if ($user instanceof UserEntityInterface === FALSE) {
      $this->getEmitter()
        ->emit(new RequestEvent(RequestEvent::USER_AUTHENTICATION_FAILED, $request));

      throw OAuthServerException::invalidCredentials();
    }

    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentifier() {
    return 'apple_login_grant';
  }

}
