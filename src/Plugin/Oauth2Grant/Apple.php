<?php

namespace Drupal\social_auth_apple_decouple\Plugin\Oauth2Grant;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\simple_oauth\Plugin\Oauth2GrantBase;
use Drupal\social_auth_apple_decouple\Grant\AppleGrant;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_apple\AppleAuthManager;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Oauth2Grant(
 *   id = "apple_login_grant",
 *   label = @Translation("Apple Login")
 * )
 */
class Apple extends Oauth2GrantBase {

  /**
   * League\OAuth2\Server\Repositories\UserRepositoryInterface.
   *
   * @var \League\OAuth2\Server\Repositories\UserRepositoryInterface
   */
  protected $userRepository;

  /**
   * League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface.
   *
   * @var \League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface
   */
  protected $refreshTokenRepository;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * AppleAuthManager definition.
   *
   * @var \Drupal\social_auth_apple\AppleAuthManager
   */
  protected $socialAuthAppleManager;

  /**
   * The network plugin manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  protected $networkManager;

  /**
   * The Social Auth user authenticator..
   *
   * @var \Drupal\social_auth\User\UserAuthenticator
   */
  protected $userAuthenticator;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Extension\ModuleHandler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    UserRepositoryInterface $user_repository,
    RefreshTokenRepositoryInterface $refresh_token_repository,
    ConfigFactoryInterface $config_factory,
    AppleAuthManager $socialAuthAppleManager,
    NetworkManager $networkManager,
    UserAuthenticator $userAuthenticator,
    AccountProxyInterface $currentUser,
    ModuleHandler $moduleHandler
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->userRepository = $user_repository;
    $this->refreshTokenRepository = $refresh_token_repository;
    $this->configFactory = $config_factory;
    $this->socialAuthAppleManager = $socialAuthAppleManager;
    $this->networkManager = $networkManager;
    $this->userAuthenticator = $userAuthenticator;
    $this->currentUser = $currentUser;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('simple_oauth.repositories.user'),
      $container->get('simple_oauth.repositories.refresh_token'),
      $container->get('config.factory'),
      $container->get('social_auth_apple.manager'),
      $container->get('plugin.network.manager'),
      $container->get('social_auth.user_authenticator'),
      $container->get('current_user'),
      $container->get('module_handler')
    );
  }

  /**
   * @return \Drupal\social_auth_apple_decouple\Grant\AppleGrant
   */
  public function getGrantType() {
    $grant = new AppleGrant(
      $this->userRepository,
      $this->refreshTokenRepository,
      $this->socialAuthAppleManager,
      $this->networkManager,
      $this->userAuthenticator,
      $this->currentUser,
      $this->moduleHandler
    );
    $settings = $this->configFactory->get('simple_oauth.settings');
    $grant->setRefreshTokenTTL(new \DateInterval(sprintf('PT%dS', $settings->get('refresh_token_expiration'))));
    return $grant;
  }

}
